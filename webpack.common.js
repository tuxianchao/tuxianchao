const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require("html-webpack-plugin");
const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        main: "./src/Main.ts",
    },
    output: {
        filename: '[name].bundle.[hash].js',
        path: __dirname + '/dist',
    },
    plugins: [
        new CleanWebpackPlugin({
            'cleanOnceBeforeBuildPatterns': 'dist'
        }),
        new HtmlWebpackPlugin({
            // 指定index.html模板.webpack会自动添加bundle.js文件
            filename: "index.html",
            template: "./index.html",
            // favicon: './index.html'
        }),
        new copyWebpackPlugin([{
                from: __dirname + '/lib', //打包的静态资源目录地址
                to: './lib' //打包到dist下面的public
            },
            {
                from: path.resolve(__dirname, "./static"),
                to: './static'
            }
        ])
    ],
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    },
}