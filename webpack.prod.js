const common = require('./webpack.common.js');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
//zip压缩
const path = require('path');
const ZipPlugin = require('zip-webpack-plugin');


module.exports = merge(common, {
    mode: "production",
    plugins: [
        new UglifyJSPlugin(),
        // // 打zip包
        // new ZipPlugin({
        //     path: path.join(__dirname, 'dist'),
        //     filename: 'game_build.zip'
        // }),
    ]
});