import CameraMoveScript from "./common/CameraMoveScript";

export default class LayaLoadModel {
    //舞台，相机，场景，灯光，精灵
    private _stage: Laya.Stage;
    private _camera: Laya.Camera;
    private _scene: Laya.Scene3D;
    private _resource: string;

    constructor(modelname: string) {
        this._resource = modelname;
        //初始化引擎
        Laya3D.init(0, 0);
        console.info(`Laya Version: ${Laya.version}`);
        Laya.stage.scaleMode = Laya.Stage.SCALE_FULL;
        Laya.stage.screenMode = Laya.Stage.SCREEN_NONE;
        Laya.Stat.show();
        this._stage = Laya.stage;

        Laya.loader.create([modelname],
            Laya.Handler.create(this, this.preLoadFinish),
            Laya.Handler.create(this, function (status: number) {
                console.info('Loading...' + (status * 100.0).toFixed(2) + '%');
            })
        );
    }

    public preLoadFinish(): void {
        //scene
        this._scene = new Laya.Scene3D();
        this._stage.addChild(this._scene);

        var directionLight: Laya.DirectionLight = this._scene.addChild(new Laya.DirectionLight()) as Laya.DirectionLight;
        directionLight.color = new Laya.Vector3(1, 1, 1);
        directionLight.transform.worldMatrix.setForward(new Laya.Vector3(1, -1, 0));

        //相机
        this._camera = new Laya.Camera(0, 0.1, 1000);
        this._camera.transform.translate(new Laya.Vector3(0, 3, 3));
        this._camera.transform.rotate(new Laya.Vector3(-30, 0, 0), true, false);
        // this._camera.transform.position = new Laya.Vector3(0, 10, 0);
        // this._camera.transform.rotate(new Laya.Vector3(90, 0, 0), false, false);
        this._camera.addComponent(CameraMoveScript);
        this._scene.addChild(this._camera);

        //加载模型
        let model: Laya.Sprite3D = Laya.loader.getRes(this._resource) as Laya.Sprite3D;
        model.transform.position = new Laya.Vector3(0, 0, 0);

        console.debug(model);
        this._scene.addChild(model);
    }
}