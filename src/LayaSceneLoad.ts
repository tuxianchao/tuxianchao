import CameraMoveScript from "./common/CameraMoveScript";

export default class LayaSceneLoad {
    //舞台，相机，场景，灯光，精灵
    private _stage: Laya.Stage;
    private _camera: Laya.Camera;
    private _scene: Laya.Scene3D;
    private _resource: string;

    constructor(scenename: string) {
        this._resource = scenename;
        //初始化引擎
        Laya3D.init(0, 0);
        console.info(`Laya Version: ${Laya.version}`);
        Laya.stage.scaleMode = Laya.Stage.SCALE_FULL;
        Laya.stage.screenMode = Laya.Stage.SCREEN_NONE;
        // Laya.Stat.show();
        this._stage = Laya.stage;

        Laya.loader.create([scenename],
            Laya.Handler.create(this, this.preLoadFinish),
            Laya.Handler.create(this, function (status: number) {
                console.info('Loading...' + (status * 100.0).toFixed(2) + '%');
            })
        );
    }

    public preLoadFinish(): void {
        //scene
        this._scene = Laya.loader.getRes(this._resource) as Laya.Scene3D;
        this._stage.addChild(this._scene);

        //相机
        this._camera = this._scene.getChildAt(0) as Laya.Camera;
        this._camera.addComponent(CameraMoveScript);
    }
}