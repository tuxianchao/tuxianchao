const common = require('./webpack.common.js');
const merge = require('webpack-merge');
const Webpack = require('webpack');
const TSLintPlugin = require('tslint-webpack-plugin');

module.exports = merge(common, {
    mode: "development",
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        host: '0.0.0.0',
        port: process.env.PORT || 3000
    }, plugins: [
        new Webpack.HotModuleReplacementPlugin({
            // Options...
            title: 'tttitle'
        }), new TSLintPlugin({
            files: ['./src/**/*.ts']
        })
    ]
});
